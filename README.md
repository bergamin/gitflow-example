# Exemplo de GitFlow

Este projeto foi criado para ajudar a explicar como funciona o GitFlow de forma simples e prática através da demonstração do gráfico final das branches.

Fluxo baseado na primeira aba do seguinte diagrama:

https://tinyurl.com/yxrpje79

# Changelog

## [16/02/2020 - 1.2.1](https://gitlab.com/bergamin/gitflow-example/-/tags/RELEASE_1.2.1)

### Novidades
* Adição do changelog ao readme (#7)

## [16/02/2020 - 1.2.0](https://gitlab.com/bergamin/gitflow-example/-/tags/RELEASE_1.2.0)

### Novidades
* Segunda nova funcionalidade adicionada (#6)
### Melhorias
* Segunda melhoria em uma funcionalidade já existente (#5)

## [16/02/2020 - 1.1.1](https://gitlab.com/bergamin/gitflow-example/-/tags/RELEASE_1.1.1)

### Correções
* Segunda correção em uma funcionalidade já existente (#4)

## [16/02/2020 - 1.1.0](https://gitlab.com/bergamin/gitflow-example/-/tags/RELEASE_1.1.0)

### Novidades
* Primeira nova funcionalidade adicionada (#2)
### Melhorias
* Primeira melhoria em uma funcionalidade já existente (#1)
### Correções
* Primeira correção em uma funcionalidade já existente (#3)

## [16/02/2020 - 1.0.0](https://gitlab.com/bergamin/gitflow-example/-/tags/RELEASE_1.0.0)

### Novidades
* Início do versionamento
